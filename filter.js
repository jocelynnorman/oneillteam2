window.addEventListener('load',function(){
    var slider = document.getElementById('pricerange');
    var display = document.getElementById('pricedisplay');
    //set sisplay to initial slider
    display.value = slider.value;
    //listen for slider changes
    slider.addEventListener ('input',function(event){
        //get value
        var val = event.target.value;
        display.value = val;
    });
 
});
  